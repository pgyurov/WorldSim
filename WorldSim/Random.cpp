#include "Random.h"
#include <random>
#include <chrono>

using namespace std;

double Random::generate_Gaussian_random_number(double mean, double variance)
{
	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	default_random_engine generator(seed);
	normal_distribution<double> distribution(mean, variance);
	_random_number = distribution(generator);

	return _random_number;
}