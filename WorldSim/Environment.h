#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H
#include <vector>

class Environment
{
protected:
	double _probability;
	double _decider_value;
	double _casualty_multipler;
	std::vector<double> _all_casualty_multipliers;

public:
	void initialise_random_seed();
	double simulate_natural_disaster();
};



#endif