#include "Population.h"
#include "Random.h"
#include <iostream>
#include <cmath>
#include <stdio.h>
#include <Windows.h>




void Population::set_initial_population(int N)
{
	_initial_population = N;
}

void Population::set_maximum_population(int N_max)
{
	_maximum_population = N_max;
}

void Population::set_growth_rate(double r)
{
	_growth_rate = r;
}

void Population::set_maximum_time(double T)
{
	_maximum_time = T;
}

void Population::set_timescaling(double t)
{
	_time_scaling = t;
}
