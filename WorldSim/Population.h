#ifndef POPULATION_H
#define POPULATION_H

class Population
{
protected:
	int _initial_population;
	int _current_population;
	int _maximum_population;
	double _growth_rate;
	double _maximum_time;
	double _time_scaling;
	double _casualty_factor;

public:
	void set_initial_population(int);
	void set_maximum_population(int);
	void set_growth_rate(double);
	void set_maximum_time(double);
	void set_timescaling(double);
};


#endif